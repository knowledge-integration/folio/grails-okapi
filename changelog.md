# Changelog

## Version 7.1.1

### Fixes
* [General]
	* Also clean up orphaned locks on instance cleanup

## Version 7.1.0

### Additions
* [General]
	* Add some locking around datasource addition.

### Changes
* [Chore]
	* Changelog - Generate the changelog

## Version 7.0.1

### Changes
* [Chore]
	* Changelog - Generate the changelog

### Fixes
* [General]
	* Changes to Hikari accounted for in connection unwrapping

## Version 7.0.0

### Additions
* [General]
	* **BREAKING** -  Upgrade to Grails 6

### Changes
* [Chore]
	* Changelog - Generate the changelog

## Version 6.0.0

### Additions
* [General]
	* Update liquibase and tweak datasource addition code

### Changes
* [Chore]
	* Changelog - Generate the changelog

## Version 6.0.0-rc.3

### Changes
* [Chore]
	* additional logging

## Version 6.0.0-rc.2

### Changes
* [Chore]
	* Changelog - Generate the changelog
	* Bump the release plugin

### Fixes
* [General]
	* SpringBoot resolution when within jar files.

## Version 6.0.0-rc.1

### Additions
* [Grails   Upgrade]
	* **BREAKING** -  Initial commit of Grails 5 upgrade
* [General]
	* ExtendedGrailsLiquibase

### Changes
* [Build]
	* Tweaks for Grails 5
* [Chore]
	* Changelog - Generate the changelog
	* (Build) Use release of web toolkit.
	* Tweaks
	* Build - Ensure no caching for toolkit changes.
	* Revert back to original filename
	* Tidy legacy comments
	* Push for Steve